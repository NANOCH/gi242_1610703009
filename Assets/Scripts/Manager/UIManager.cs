﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Manager
{
   public class UIManager : MonoSingleton<UIManager>
   {
      public static UIManager Instance { get; private set; }

      private void Awake()
      {
         if (Instance == null)
         {
            Instance = this;
         }

         DontDestroyOnLoad(this);
      }
      public void Start()
      {
         SceneManager.sceneLoaded += OnSceneLoaded;

      }

      void OnSceneLoaded(Scene scene, LoadSceneMode mode)
      {

      }

   }
}
