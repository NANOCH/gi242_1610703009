using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Spaceship
{
    public class EnemySpaceship : Basespaceship, IDamagable
    {
        [SerializeField] private PlayerSpaceship player;
        bool stopCoroutine = true;
        public event Action OnExploded;

        public int currentHP;


        private IEnumerator coroutine;

        public void Init(int hp, float speed, Bullet bullet)
        {
            base.Init(hp, speed, defaultBullet);
            currentHP = Hp;
        }

        public void TakeHit(int damage)
        {
            currentHP -= damage;

            if (currentHP > 0)
            {
                return;
            }

            Explode();
        }

        public void Explode()
        {
            Debug.Assert(Hp > 0, "HP is more than zero");
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }

        public override void Fire()
        {
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init();
            SoundManager.Instance.Play(audioSource, SoundManager.Sound.Fire);
        }

        public override void Fire2()
        {
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.EnemyShoot();
            SoundManager.Instance.Play(audioSource, SoundManager.Sound.Fire);

        }

        public void Update()
        {
            if (stopCoroutine == true)
            {
                StartCoroutine(WaitToFire());
                stopCoroutine = false;
            }

        }

        IEnumerator WaitToFire()
        {
            yield return new WaitForSeconds(1);

            Fire2();
            stopCoroutine = true;
        }

    }
}
